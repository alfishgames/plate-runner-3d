﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowablePlate : MonoBehaviour
{
    //GameObjects, Transforms & Rigidbodies
    public GameObject enemy;
    public GameObject player;
    public GameObject brokenPlate;

    //Script Variables
    float lerpValue;
    public float lerpRatio;
    public Vector3 plateSpawnOffset;

    private void Start()
    {
        player = GameObject.Find("Player");
        enemy = GameObject.Find("Enemy");
    }

    private void LateUpdate()
    {
        PlayerThrowingPlate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            DestroyPlate();
        }

    }

    void PlayerThrowingPlate()
    {
        lerpValue += Time.deltaTime / lerpRatio;
        transform.position = Vector3.Lerp(player.transform.position + plateSpawnOffset, enemy.transform.position, lerpValue);
        transform.LookAt(enemy.transform, Vector3.left);
    }
    
    void DestroyPlate()
    {
        Destroy(gameObject);
        Instantiate(brokenPlate, enemy.transform.position, Quaternion.Euler(1f, 3f, 8f));
    }
}
