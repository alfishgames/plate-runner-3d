﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTPlate : MonoBehaviour
{
    //GameObjects, Transforms & Rigidbodies
    public GameObject enemy;
    public GameObject player;
    public GameObject brokenPlate;

    //Script Variables
    float lerpValue;
    public float lerpRatio;
    public Vector3 plateSpawnOffset;

    private void Start()
    {
        player = GameObject.Find("Player");
        enemy = GameObject.Find("Enemy");
    }

    private void LateUpdate()
    {
        EnemyThrowingPlate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            DestroyPlate();
        }
    }


    void EnemyThrowingPlate()
    {
        lerpValue += Time.deltaTime / lerpRatio;
        transform.position = Vector3.Lerp(enemy.transform.position + plateSpawnOffset, player.transform.position, lerpValue);
        transform.LookAt(enemy.transform, Vector3.left);
    }

    void DestroyPlate()
    {
        Instantiate(brokenPlate, player.transform.position, Quaternion.Euler(1f, 3f, 8f));
        Destroy(gameObject);
        
    }
}
