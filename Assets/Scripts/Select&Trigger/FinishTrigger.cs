﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishTrigger : MonoBehaviour
{
    public GameObject finishedMenuUI;
    public Text winStateText;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RunnerTable_Player"))
        {
            Debug.Log("Finished!");
            winStateText.text = "Congratulations! \n You Win.";
            Time.timeScale = 0f;
            finishedMenuUI.SetActive(true);
        }
        else if (other.CompareTag("RunnerTable_Enemy"))
        {
            Debug.Log("Finished!");
            winStateText.text = "Ooh No! \n You Lose.";
            Time.timeScale = 0f;
            finishedMenuUI.SetActive(true);
        }
    }

    public void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("PlateRunner");
    }
}
