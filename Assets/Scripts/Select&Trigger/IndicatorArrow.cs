﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorArrow : MonoBehaviour
{
    //Calling Scripts
    PlayerBehaviour playerBehaviour;

    //Indicator Variables
    #region      Indicator Variables
    public bool isMoving;
    private GameObject myParentPosition;
    public float speed;
    public GameObject indicatorArrow;
    public float indicatorOffsetY = -2.3f, indicatorOffsetZ = 10.6f;
    Vector3 indicatorStartPoint = new Vector3(-4.4f, 0.7f, -0.1f);
    public bool isRed, isOrange, isYellow, isLGreen, isDGreen;
    #endregion

    //Some Variables
    public int state;


    public void Start()
    {
        indicatorArrow.transform.position = indicatorStartPoint;
        isMoving = true;
        myParentPosition = gameObject.transform.parent.gameObject;
    }

    public void Update()
    {
        if (isMoving)
        {                       
            Vector3 indicatorMover = new Vector3(Mathf.PingPong(Time.time * speed, 8.8f) - 4.4f, myParentPosition.transform.localPosition.y + indicatorOffsetY, myParentPosition.transform.localPosition.z + indicatorOffsetZ);       
            indicatorArrow.transform.localPosition = indicatorMover;
            GetState();
        }
        
    }

    public void GetState()
    {
        if (isRed) state = 1;
        else if (isOrange) state = 2;
        else if (isYellow) state = 3;
        else if (isLGreen) state = 4;
        else if (isDGreen) state = 5;
    }
    
    public void StopIndicator()
    {
        isMoving = false;
        GetState();
    }
    public void StartIndicator()
    {
        isMoving = true;
    }
    
    public void OnTriggerEnter (Collider col)
    {
        if (col.gameObject.CompareTag("Red Indicator")) 
        {
            isRed = true;
            isOrange = false;
            isYellow = false;
            isLGreen = false;
            isDGreen = false;
        }

        else if (col.gameObject.CompareTag("Orange Indicator"))
        {
            isRed = false;
            isOrange = true;
            isYellow = false;
            isLGreen = false;
            isDGreen = false;
        }

        else if (col.gameObject.CompareTag("Yellow Indicator"))
        {
            isRed = false;
            isOrange = false;
            isYellow = true;
            isLGreen = false;
            isDGreen = false;
        }

        else if (col.gameObject.CompareTag("Light Green Indicator"))
        {
            isRed = false;
            isOrange = false;
            isYellow = false;
            isLGreen = true;
            isDGreen = false;
        }

        else if (col.gameObject.CompareTag("Dark Green Indicator"))
        {         
            isRed = false;
            isOrange = false;
            isYellow = false;
            isLGreen = false;
            isDGreen = true;
        }
    }
}
