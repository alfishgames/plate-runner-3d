﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Calling Scripts
    IndicatorArrow indicatorArrow;
    PlayerBehaviour playerBehaviour;

    //GameObjects, Transforms & Rigidbodies
    public Rigidbody rb;

    //Script Variables
    public float movementForce;


    void Start()
    {
        indicatorArrow = GameObject.FindWithTag("Player_Indicator").GetComponent<IndicatorArrow>();
        playerBehaviour = GameObject.FindWithTag("Player").GetComponent<PlayerBehaviour>();
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            playerBehaviour.States();
        }       
    }
}
