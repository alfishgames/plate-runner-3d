﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour
{
    //Calling Scripts

    //GameObjects & Rigidbodies
    public GameObject enemy;
    public GameObject throwablePlate;
    public GameObject player;
    public Rigidbody enemyTableRb;

    //Script Variables
    public float speed = 1f;
    float perfectTimer, tableForce;
    public float randomTimer;
    int state;

    bool isAttacked, needCatch;

    public Slider slider;

    //Vectors
    Vector3 vectorFWD = Vector3.forward;

    void Start()
    {
        #region Calling Scripts
        #endregion
        speed = 6f; 
    }

    void Update()
    {
        EnemyAttack();
        RandomThrowPlate();
        ReCastAttack();
        CatchPlayer();
        ProgressCalculator(1);
    }
    private void FixedUpdate()
    {
        EnemyMovement();
    }

    void EnemyMovement()
    {
        enemyTableRb.velocity = vectorFWD * Time.deltaTime * 50 * speed;
    }

    public void EnemyThrowPlate()
    {
        GameObject tp = Instantiate(throwablePlate, enemy.transform.position, Quaternion.Euler(0f, 90f, 90f));
    }

    void RandomThrowPlate()
    {
        randomTimer += Time.deltaTime;

        if (randomTimer >= Random.Range(1f, 4f)) 
        {
            EnemyThrowPlate();
            randomTimer = 0f;
        }    
    }

    public void EnemyAttack()
    {
        if (isAttacked)
        {
            state = 1 + Random.Range(0, 4);
            States();
            isAttacked = false;
        }
    }

    void ReCastAttack()
    {
        perfectTimer += Time.deltaTime;

        if (perfectTimer >= Random.Range(1f, 1.7f))
        {
            perfectTimer = 0f;
            isAttacked = true;
        }
    }
    public void States()
    {
        if (state == 1)
        {
            speed = 5f;
            tableForce = 5f;
            enemyTableRb.velocity = vectorFWD * tableForce;
        }
        if (state == 2)
        {
            speed = 6f;
            tableForce = 6f;
            enemyTableRb.velocity = vectorFWD * tableForce;
        }
        if (state == 3)
        {
            speed = 7f;
            tableForce = 7f;
            enemyTableRb.velocity = vectorFWD * tableForce;
            EnemyThrowPlate();
        }
        if (state == 4)
        {
            speed = 9f;
            tableForce = 8f;
            enemyTableRb.velocity = vectorFWD * tableForce;
            EnemyThrowPlate();
        }
        if (state == 5)
        {
            speed = 10f;
            tableForce = 10f;
            enemyTableRb.velocity = vectorFWD * tableForce;
            EnemyThrowPlate();
        }

    }

    void CatchPlayer()
    {
        Vector3 Distance = player.transform.position - enemy.transform.position;
        float distanceZ = Distance.z;

        if (distanceZ <= 2f)
        {
            needCatch = false;
        }

        else if (distanceZ >= 3f)
        {
            needCatch = true;
        }


        if (needCatch)
        {
            perfectTimer = 0f;
            speed += .05f;
            if(speed >= 18f)
            {
                speed = 18f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Throwable_Plate"))
        {
            TakeDamage();
        }
    }
    void TakeDamage()
    {
        #region Hit Force
        Vector3 GetDirection = player.transform.position - enemy.transform.position;
        float getDirection = GetDirection.x;
        float force = 1f;
        Vector3 direction = Vector3.zero;

        if (getDirection >= 0) direction = Vector3.left;
        else if (getDirection <= 0) direction = Vector3.right;

        enemyTableRb.AddForce(direction * force, ForceMode.Impulse);
        enemyTableRb.AddForce(Vector3.up * 2f, ForceMode.Impulse);
        #endregion
        Debug.Log(transform.name + " takes damage");
        speed -= 5f;
    }

    void ProgressCalculator(int sceneLevel)
    {
        Vector3 playerPoint = player.transform.position;
        float startZ, finishZ, enemyZ;

        if (sceneLevel == 1)
        {
            Vector3 startPoint = new Vector3(0.94f, 0.32f, -6.968f);
            Vector3 finishPoint = new Vector3(0.3689378f, 0.09157622f, 230.6447f);
            startZ = startPoint.z;
            finishZ = finishPoint.z;
            enemyZ = enemy.transform.position.z;
            slider.maxValue = finishZ - startZ;
            slider.value = enemyZ - startZ;
        }
    }
}
