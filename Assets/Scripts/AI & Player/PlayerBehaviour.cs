﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    public static PlayerBehaviour instance;


    //Calling Scripts
    IndicatorArrow indicatorArrow;

    //GameObjects, Transforms & Rigidbodies
    public GameObject player;
    public GameObject enemy;
    public GameObject throwablePlate;
    public Rigidbody tableRb;
    public Transform tableTransform;

    //Script Variables
    int state, perfectCount;
    float timer;
    public float speed;
    public float stateSpeed;
    bool isPerfect;
    public Slider slider, perfectSlider;
    public Text perfectText;

    //Vectors
    Vector3 vectorFWD = Vector3.forward;

    public void Start()
    {
        instance = this;
        #region Calling Scripts
        indicatorArrow = GameObject.FindWithTag("Player_Indicator").GetComponent<IndicatorArrow>();
        #endregion  
        perfectText.enabled = false;
        perfectSlider.gameObject.SetActive(false);
        speed = stateSpeed;
        speed = 4f;

    }
    public void Update()
    {
        state = indicatorArrow.state;
        ProgressCalculator(1);
        PerfectState();
    }
    private void FixedUpdate()
    {
        TableMovement();
    }

    void TableMovement()
    {
        tableRb.velocity = vectorFWD * Time.deltaTime * 50 * speed;
        if(stateSpeed <= 0f)
        {
            stateSpeed = 2f;
            speed = stateSpeed;
        }
    }

    public void ThrowPlate()
    {
        GameObject tp = Instantiate(throwablePlate, player.transform.position, Quaternion.Euler(0f, 90f, 90f));
    }


    public void TakeDamage()
    {
        #region Hit Force
        Vector3 GetDirection = enemy.transform.position - player.transform.position;
        float getDirection = GetDirection.x;
        float force = 1f;
        Vector3 direction = Vector3.zero;

        if (getDirection >= 0) direction = Vector3.left;
        else if (getDirection <= 0) direction = Vector3.right;

        tableRb.AddForce(direction * force, ForceMode.Impulse);
        tableRb.AddForce(Vector3.up * 2f, ForceMode.Impulse);
        #endregion
        Debug.Log(transform.name + " takes damage");
        stateSpeed -= 4f;
    }

    public void States()
    {
        if (state == 1)
        {
            stateSpeed = 4f;
            speed = stateSpeed;
        }
        if (state == 2)
        {
            stateSpeed = 5f;
            speed = stateSpeed;
        }
        if (state == 3)
        {
            stateSpeed = 6f;
            speed = stateSpeed;
        }
        if (state == 4)
        {
            stateSpeed = 9f;
            speed = stateSpeed;
        }
        if (state == 5)
        {
            stateSpeed = 10f;
            speed = stateSpeed;
            ThrowPlate();
            isPerfect = true;
            perfectCount++;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Throwable_Plate_Enemy"))
        {
            TakeDamage();
        }
    }

    void PerfectState()
    {
        perfectSlider.maxValue = 99f;

        if (perfectCount == 0) perfectSlider.gameObject.SetActive(false);

        if (perfectCount == 1) 
        {
            perfectSlider.value += Time.deltaTime * 50f;
            perfectText.text = "Perfect!";
            speed += 0.04f;
            if (perfectSlider.value >= 33f)
            {
                speed = stateSpeed;
                perfectSlider.gameObject.SetActive(false);
                perfectSlider.value = 33f;
            }
        }

        if (perfectCount == 2)
        {
            perfectSlider.value += Time.deltaTime * 50f;
            perfectText.text = "Perfect! x2";
            speed += 0.05f;
            if (perfectSlider.value >= 66f)
            {
                speed = stateSpeed;
                perfectSlider.gameObject.SetActive(false);
                perfectSlider.value = 66f;
            }
        }

        if (perfectCount == 3)
        {
            perfectSlider.value += Time.deltaTime * 50f;
            perfectText.text = "Perfect! x3";
            speed += 0.06f;
            if (perfectSlider.value >= 99f)
            {
                speed = stateSpeed;
                perfectSlider.gameObject.SetActive(false);
                perfectSlider.value = 0f;
                perfectCount = 0;
            }

        }

        if (isPerfect)
        {
            perfectText.enabled = true;
            perfectSlider.gameObject.SetActive(true);

            timer += Time.deltaTime;
            if (timer >= 1f)
            {
                perfectText.enabled = false;
                isPerfect = false;
                timer = 0;
            }

        }
    }

    void ProgressCalculator(int sceneLevel)
    {
        Vector3 playerPoint = player.transform.position;
        float startZ, finishZ, playerZ;

        if(sceneLevel == 1)
        {
            Vector3 startPoint = new Vector3(0.94f, 0.32f, -6.968f);
            Vector3 finishPoint = new Vector3(0.3689378f, 0.09157622f, 230.6447f);
            startZ = startPoint.z;
            finishZ = finishPoint.z;
            playerZ = player.transform.position.z;
            slider.maxValue = finishZ - startZ;
            slider.value = playerZ - startZ;
        }
    }

}
